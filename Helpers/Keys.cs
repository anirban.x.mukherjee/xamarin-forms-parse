﻿using System;

namespace ParseSample.Helpers
{
    public class Keys
    {
        public static readonly string ParseAppKey = "2lW6Pdnjn72fjQsPojhcNhKCNwD8GKDQrLrl1PVx"; 
        public static readonly string ParseDotNetKey = "ZPhS5LBn6yuO7Z9IjPdRlodTVDq0TQ4qov8geWYm";
        public static readonly string ParseServer = "https://parseapi.back4app.com/";
    }
}

