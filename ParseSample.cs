﻿using System;

using Xamarin.Forms;
using Parse;
using System.Threading.Tasks;

namespace ParseSample
{
    public class App : Application
    {
        public bool isParseOK = false;

        public App()
        {

            ParseClient.Initialize(new ParseClient.Configuration
            {
                ApplicationId = Helpers.Keys.ParseAppKey,
                WindowsKey = Helpers.Keys.ParseDotNetKey,
                Server = Helpers.Keys.ParseServer
            });

            Console.WriteLine("Initiating Parse server connectivity check async ...");
            CheckParseServerConnection();
       
            // The root page of your application 
            MainPage = new NavigationPage(new Pages.SignUpPage());


        }
        // This method is meant to terminate the async Task chain
        public async void CheckParseServerConnection()
        {
            isParseOK = await ValidateParse();
        }

        private async Task<bool> ValidateParse()
        {
            try
            {
                ParseObject testObject = new ParseObject("Test");
                Console.WriteLine("ParseAccess: ValidateParse(): Will Save");
                await testObject.SaveAsync();
                Console.WriteLine("ParseAccess: ValidateParse(): Saved with objectId: {0}. Will delete", testObject.ObjectId);
                await testObject.DeleteAsync();
                Console.WriteLine("ParseAccess: ValidateParse(): Deleted objectId: {0}", testObject.ObjectId);
            }
            catch (Exception e)
            {
                Console.WriteLine("ParseAccess: ValidateParse(): CONNECTION STATUS FAIL: {0}", e.ToString());
                //Console.WriteLine(new System.Diagnostics.StackTrace());
                return false;
            }
            Console.WriteLine("ParseAccess: ValidateParse(): success");
            return true;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

