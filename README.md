## Xamarin.Forms sample using Back4App.com, a Parse server


### Sample app that illustrates how a Xamarin.Forms app can use a Parse backend on Back4App.com

There are three projects :-  

1. **ParseSample**: a shared project which includes the UI pieces as well as the pieces that interact with Back4App  
    - Helpers/Keys.cs: API keys for Back4App 
    - Pages/ SignupPage.xaml and SignupPage.xaml.cs: UI display for Signup page  
	- ViewModels: Code that acts as the Model for the UI and invokes the Parse call *user.SignUpAsync()* to sign up a user  
	- ParseSample.cs: App class  
	
2. **ParseSample.Droid**: Android App. Note *MainActivity.cs*

3. **ParseSample.iOS**: iOS App. Note *Main.cs*

For 2 and 3 , the most important aspect is the *References*. Both projects have a reference to the shared project **ParseSample** and also to the various platform-specific libraries for Android and iOS.  *Parse.Android* and *Parse.iOS* are also among these. Otherwise, they are like any other Xamarin.Forms app.

### Notes
- Use ```Build -> Build Solution/Rebuild Solution``` for building the projects
- Testing has not been carried out on iOS emulator yet
- ```Debug -> Start without Debugging``` is a handy way to run the app on emulator
- *Device Log* window can be opened by using ```Tools -> Android/iOS -> Device Log```. The emulator should be running and you should select it in the *Choose Device* dropdown.
- Debug messages are printed from the code using *Console.WriteLine()*. As a result, in the Android emululator (and also in iOS emulator presumably), they will appear in the Device Log with tag *mono-stdout*.  
So if you enter *mono-stdout* into the search box in the *Device Log* window, it will list out the app log messages. 


### References
- https://github.com/MikeCodesDotNET/Xamarin.Forms-Parse-Sample